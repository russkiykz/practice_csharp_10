﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PracticeOfInterfaces
{
    public class Student : IStudent
    {
        public string Name { get; set;}
        public string FullName { get; set; }
        public int[] Grades { get; set; }

        
        public Student(string name, string fullName, int[] grades)
        {
            Name = name;
            FullName = fullName;
            Grades = grades;
        }

        public double GetAvgGrade()
        {
            double sumGrades = 0;
            for (int i = 0; i < Grades.Length; i++)
            {
                sumGrades += Grades[i];
            }
            return sumGrades / Grades.Length;
        }

        public string GetFullName()
        {
            return FullName;
        }

        public string GetName()
        {
            return Name;
        }
    }
}
