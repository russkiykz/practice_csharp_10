﻿using System;

namespace PracticeOfInterfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            Student student = new Student("Никита", "Болтушкин", new int[] { 3, 3, 4, 5, 4 });
            Console.WriteLine($"{student.GetName()} {student.GetFullName()} - средняя оценка: {student.GetAvgGrade()}");
        }
    }
}
